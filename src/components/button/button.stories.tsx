import React from 'react';
import { Button, BUTTON_TYPE, BUTTON_SIZE } from './index';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';

export default {
  title: 'Button',
  component: Button,
  decorators: [withKnobs]
};

export const knobsBtn = () => (
  <Button
    size={select<BUTTON_SIZE>('size', BUTTON_SIZE, BUTTON_SIZE.default)}
    href={text('hrefText', '')}
    isLink={boolean('isLink', false)}
    loadingText={text('loadingTEXT', 'I AM LOADING')}
    isLoading={boolean('isLoading', false)}
    disabled={boolean('disabled', false)}
    buttonType={select<BUTTON_TYPE>('buttonType', BUTTON_TYPE, BUTTON_TYPE.primary)}
  >
    {text('childrenText', 'Hello Storybook')}
  </Button>
);

export const buttons = () => (
  <>
    <Button buttonType={BUTTON_TYPE.primary}>Primary</Button>
    <Button buttonType={BUTTON_TYPE.secondary}>Secondary</Button>
    <Button buttonType={BUTTON_TYPE.tertiary}>Tertiary</Button>
    <Button buttonType={BUTTON_TYPE.outline}>Outline</Button>
    <Button buttonType={BUTTON_TYPE.primaryOutline}>Outline primary</Button>
    <Button buttonType={BUTTON_TYPE.secondaryOutline}>Outline secondary</Button>
    <div style={{ background: 'grey', display: 'inline-block' }}>
      <Button buttonType={BUTTON_TYPE.inversePrimary}>Primary inverse</Button>
    </div>
    <div style={{ background: 'grey', display: 'inline-block' }}>
      <Button buttonType={BUTTON_TYPE.inverseSecondary}>Secondary inverse</Button>
    </div>
    <div style={{ background: 'grey', display: 'inline-block' }}>
      <Button buttonType={BUTTON_TYPE.inverseOutline}>Outline inverse</Button>
    </div>
  </>
);

export const sizes = () => (
  <>
    <Button buttonType={BUTTON_TYPE.primary}>Default</Button>
    <Button buttonType={BUTTON_TYPE.primary} size={BUTTON_SIZE.small}>
      Small
    </Button>
  </>
);

export const loading = () => (
  <>
    <Button buttonType={BUTTON_TYPE.primary} isLoading>
      Primary
    </Button>
    <Button buttonType={BUTTON_TYPE.secondary} isLoading>
      Secondary
    </Button>
    <Button buttonType={BUTTON_TYPE.tertiary} isLoading>
      Tertiary
    </Button>
    <Button buttonType={BUTTON_TYPE.outline} isLoading>
      Outline
    </Button>
    <Button buttonType={BUTTON_TYPE.outline} isLoading loadingText="Custom...">
      Outline
    </Button>
  </>
);

export const disabled = () => (
  <>
    <Button buttonType={BUTTON_TYPE.primary} disabled>
      Primary
    </Button>
    <Button buttonType={BUTTON_TYPE.secondary} disabled>
      Secondary
    </Button>
    <Button buttonType={BUTTON_TYPE.tertiary} disabled>
      Tertiary
    </Button>
    <Button buttonType={BUTTON_TYPE.outline} disabled>
      Outline
    </Button>
  </>
);

export const link = () => (
  <>
    <Button buttonType={BUTTON_TYPE.primary} isLink href="/">
      Primary
    </Button>
    <Button buttonType={BUTTON_TYPE.secondary} isLink href="/">
      Secondary
    </Button>
    <Button buttonType={BUTTON_TYPE.tertiary} isLink href="/">
      Tertiary
    </Button>
    <Button buttonType={BUTTON_TYPE.outline} isLink href="/">
      Outline
    </Button>
  </>
);
