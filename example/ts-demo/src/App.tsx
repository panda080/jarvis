import React from "react";
import { GlobalStyle, Button, BUTTON_TYPE } from "@blue080/jarvis";

function App() {
  return (
    <div className="App">
      <GlobalStyle/>
      <Button buttonType={BUTTON_TYPE.primary}>2222</Button>
    </div>
  );
}

export default App;
